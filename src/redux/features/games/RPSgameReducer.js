import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  gameInfo: "",
  isGameStarted: false,
  isGameFinished: false,
  currentRound: 0,
  playerChoice: "",
  botChoice: "",
  playerwinRound: 0,
  botwinRound: 0,
  playerScore: 0,
  botScore: 0,
};

const RPSgameSlice = createSlice({
  name: "rpsgame",
  initialState,
  reducers: {
    playRPSGame: (state, action) => {
      const { playerChoice, botChoice, gameInfo } = action.payload;
      state.gameInfo = gameInfo;
      state.playerChoice = playerChoice;
      state.botChoice = botChoice;
      state.isGameStarted = true;
      state.isGameFinished = false;
      state.currentRound += 1;
    },
    updateRound: (state, action) => {
      const { playerwinRound, botwinRound } = action.payload;
      state.playerwinRound += playerwinRound;
      state.botwinRound += botwinRound;
    },
    updateScore: (state, action) => {
      const { playerScore, botScore } = action.payload;
      state.playerScore += playerScore;
      state.botScore += botScore;
    },
    updateInfo: (state, action) => {
      state.gameInfo = action.payload;
    },
    finishGame: (state, action) => {
      state.score = action.payload;
      state.isGameFinished = true;
      state.isGameStarted = false;
      state.gameInfo = "Game concluded, restart the game to start again";
    },
    resetGame: (state) => {
      state.gameInfo = "";
      state.isGameStarted = false;
      state.isGameFinished = false;
      state.playerwinRound = 0;
      state.botwinRound = 0;
      state.currentRound = 0;
      state.playerChoice = "";
      state.botChoice = "";
    },
  },
});

export const {
  playRPSGame,
  updateScore,
  updateRound,
  updateInfo,
  finishGame,
  resetGame,
} = RPSgameSlice.actions;

export default RPSgameSlice.reducer;
