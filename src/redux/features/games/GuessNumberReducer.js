import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  gameInfo: "",
  isGameStarted: false,
  isGameFinished: false,
  maxTries: 10,
  currentTries: 0,
  guess: 0,
  randomNumber: 0,
};

const GuessNumberSlice = createSlice({
  name: "numbergame",
  initialState,
  reducers: {
    playGuess: (state, action) => {
      state.gameInfo = action.payload;
      state.isGameStarted = true;
      state.isGameFinished = false;
    },
    updateRandomNumber: (state, action) => {
      state.randomNumber = action.payload;
    },
    updateGuess: (state, action) => {
      state.guess = action.payload;
    },
    updateTries: (state) => {
      state.currentTries += 1;
    },
    updateInfo: (state, action) => {
      state.gameInfo = action.payload;
    },
    finishGame: (state, action) => {
      state.gameInfo = action.payload;
      state.isGameFinished = true;
    },
    resetGame: (state) => {
      state.gameInfo = "";
      state.isGameStarted = false;
      state.isGameFinished = false;
      state.currentTries = 0;
      state.randomNumber = Math.floor(Math.random() * 100) + 1;
      state.guess = 0;
    },
  },
});

export const {
  playGuess,
  updateInfo,
  updateRandomNumber,
  updateTries,
  updateGuess,
  finishGame,
  resetGame,
} = GuessNumberSlice.actions;

export default GuessNumberSlice.reducer;
