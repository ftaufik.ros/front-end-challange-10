import DummyGame from "@/components/GamesPage/DummyGames";
import GuessLogic from "@/components/GamesPage/GuessNumber/GuessLogic";
import RPSLogic from "@/components/GamesPage/RPSGame/RPSLogic";
import Navbar from "@/components/Navbar/Navbar";

function playPage({ params }) {
  let id = params.id;

  function RenderGame(id) {
    let render = null;
    switch (id) {
      case "1":
        render = <RPSLogic />;
        break;
      case "2":
        render = <GuessLogic />;
        break;
      case "3":
        render = <DummyGame />;
        break;
      case "4":
        render = <DummyGame />;
        break;
      default:
        render = "404 - Game Not Found";
        break;
    }
    return render;
  }
  return (
    <div>
      <Navbar />
      {RenderGame(id)}
    </div>
  );
}

export default playPage;
