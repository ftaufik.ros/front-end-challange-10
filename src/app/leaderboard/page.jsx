import Navbar from "@/components/Navbar/Navbar";
import LeaderboardList from "@/components/LeaderboardPage/LeaderboardList";

export default function Example() {
  return (
    <div>
      <Navbar />
      <div className="relative isolate overflow-hidden bg-gray-900 pt-20 sm:pb-9 px-3 text-white"
      style={{minHeight: "100vh"}}>
        <div
          className="hidden sm:absolute sm:-top-10 sm:right-1/2 sm:-z-10 sm:mr-10 sm:block sm:transform-gpu sm:blur-3xl"
          aria-hidden="true"
        >
          <div
            className="aspect-[1097/845] w-[68.5625rem] bg-gradient-to-tr from-[#e92ee9] to-[#c1ff6f] opacity-20"
            style={{
              clipPath:
                "polygon(74.1% 44.1%, 100% 61.6%, 97.5% 26.9%, 85.5% 0.1%, 80.7% 2%, 72.5% 32.5%, 60.2% 62.4%, 52.4% 68.1%, 47.5% 58.3%, 45.2% 34.5%, 27.5% 76.7%, 0.1% 64.9%, 17.9% 100%, 27.6% 76.8%, 76.1% 97.7%, 74.1% 44.1%)",
            }}
          />
        </div>
        <div
          className="absolute -top-52 left-1/2 -z-10 -translate-x-1/2 transform-gpu blur-3xl sm:top-[-28rem] sm:ml-16 sm:translate-x-0 sm:transform-gpu"
          aria-hidden="true"
        >
          <div
            className="aspect-[1097/845] w-[68.5625rem] bg-gradient-to-tr from-[#ff6e99] to-[#59e6ff] opacity-20"
            style={{
              clipPath:
                "polygon(74.1% 44.1%, 100% 61.6%, 97.5% 26.9%, 85.5% 0.1%, 80.7% 2%, 72.5% 32.5%, 60.2% 62.4%, 52.4% 68.1%, 47.5% 58.3%, 45.2% 34.5%, 27.5% 76.7%, 0.1% 64.9%, 17.9% 100%, 27.6% 76.8%, 76.1% 97.7%, 74.1% 44.1%)",
            }}
          />
        </div>
        <div className="font-bold text-center pb-10">
          <h5 className="opacity-50">Leaderboard</h5>
          <h1 className="text-2xl md:text-4xl md:w-96 w-64 mt-5 mx-auto leading-normal">
            Let&#39;s Compete With Other Players
          </h1>
        </div>
        <LeaderboardList />
      </div>
    </div>
  );
}
