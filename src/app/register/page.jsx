import Navbar from "@/components/Navbar/Navbar";
import Register from "@/components/Register/Register";

// export async function getData({req, res}) {
//   const router = useRouter()
//   const cookies = req.headers.cookie;
//   if (cookies) {
//     const userIdCookie = cookies
//       .split(';')
//       .find((cookie) => cookie.trim().startsWith('userId='));
//     if (userIdCookie) {
//       // res.writeHead(302, { Location: '/home' });
//       // res.end();
//       router.push('/home')
//     }
//   }

//   return {
//     props: {},
//   }
// }
export default function RegisterPage() {
  return (
    <>
      <Navbar />
      <Register />
    </>
  );
}
