"use client";

import React, { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../../redux/hooks";
import { fetchData } from "../../redux/features/listgame";
import { fetchUserGameplay } from "../../redux/features/playChecker";
import background from "../../../public/assets/form-bg.jpg";
import Link from "next/link";
import Cookies from "js-cookie";
import axios from "axios";

export default function GameListPage() {
  const dispatch = useAppDispatch();
  const userGame = useAppSelector((state) => state.playChecker.userGameplay);
  const games = useAppSelector((state) => state.listGameSlice.data);
  const loading = useAppSelector((state) => state.listGameSlice.loading);
  const error = useAppSelector((state) => state.listGameSlice.error);

  const fetchGame = async () => {
    try {
      const uid = Cookies.get("userId");
      if (uid) {
        const resGame = await axios.get(
          `https://gatots-api.vercel.app/api/v1/score/get/${uid}`
        );
        console.log(resGame);
      }
    } catch (error) {
      console.log(error.message);
    }
  };

  const uid = Cookies.get("userId");
  useEffect(() => {
    dispatch(fetchData());
    dispatch(fetchUserGameplay(uid));
  }, [dispatch, uid]);

  const hasPlayedGame = (gameId) => {
    return userGame.some((game) => game.gameId === gameId);
  };
  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  return (
    <div style={styles.container}>
      <div
        className="flex flex-col items-center justify-center min-h-screen bg-cover bg-center"
        style={{ backgroundImage: `url(${background})` }}
      >
        <h2 className="text-2xl font-bold mb-4">GAMES</h2>
        <div className="flex flex-wrap justify-center">
          {games.map((game) => (
            <div
              key={game.id}
              className="w-1/4 m-4 p-6 bg-gray-600 rounded-lg text-center"
            >
              <div
                className="w-full h-48 mb-4 rounded-lg"
                style={{
                  backgroundImage: `url(${game.thumbnail})`,
                  backgroundSize: "cover",
                  backgroundPosition: "center",
                }}
              />
              <h3 className="text-lg font-bold mb-2">{game.title}</h3>
              {hasPlayedGame(game.id) && (
                <p className="text-white text-lg">
                  YOU ALREADY PLAYED THIS GAME BEFORE
                </p>
              )}
              <p>{game.description}</p>
              <div className="mt-4">
                <Link
                  href={`/games/${game.id}/play`}
                  className="inline-block px-4 py-2 mr-2 bg-black text-white rounded-full font-bold transition-colors duration-300 hover:bg-gray-800"
                >
                  Play
                </Link>
                <Link href={`/games/${game.id}`} className="text-white">
                  Learn More
                </Link>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

const styles = {
  container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    height: "100vh",
    backgroundImage: `url(${background.src})`,
    backgroundSize: "cover",
    backgroundPosition: "center",
  },
};
