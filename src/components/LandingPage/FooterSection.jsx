export default function FooterSection() {
  return (
    <div className="bg-gray-800 text-sm p-4 text-center text-white">
      Copyright © 2023 Gatot Sprinter All Rights reserved.
    </div>
  );
}
