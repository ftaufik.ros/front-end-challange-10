"use client";

import { fetchLeaderboard } from "@/redux/features/leaderboardListReducer";
import { useAppDispatch, useAppSelector } from "@/redux/hook";
import { useEffect } from "react";

const people = [
  {
    name: "Leslie Alexander",
    role: "Legendary",
  },
  {
    name: "Peep Alexander",
    role: "Platinum",
  },
  {
    name: "Leslie Pansenada",
    role: "Magical",
  },
  {
    name: "1Leslie Alexander",
    role: "Legendary",
  },
  {
    name: "1Peep Alexander",
    role: "Platinum",
  },
  {
    name: "1Leslie Pansenada",
    role: "Magical",
  },
];

export default function LeaderboardSection() {
  const dispatch = useAppDispatch();

  const leaderboards = useAppSelector((state) => state.leaderboard.data);
  const newLead = [...leaderboards];
  console.log(leaderboards);

  useEffect(() => {
    dispatch(fetchLeaderboard());
  }, [dispatch]);

  return (
    <div className="relative isolate overflow-hidden bg-gray-300 py-16 sm:py-24 lg:py-32">
      <div className="mx-auto grid max-w-7xl gap-x-8 gap-y-20 px-6 lg:px-8 xl:grid-cols-3">
        <div className="max-w-2xl">
          <h2 className="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">
            Meet Our Top Scores
          </h2>
          <p className="mt-6 text-lg leading-8 text-gray-600">
            Libero fames augue nisl porttitor nisi, quis. Id ac elit odio vitae
            elementum enim vitae ullamcorper suspendisse.
          </p>
        </div>

        <ul
          role="list"
          className="grid gap-x-8 gap-y-12 sm:grid-cols-2 sm:gap-y-16 xl:col-span-2 ml-10"
        >
          {newLead
            .filter((data) => data.id === 1)
            .sort((a, b) => b.score - a.score)
            .map((data, index) => (
              <li key={index}>
                <div className="flex items-center gap-x-6">
                  <div>
                    <h3 className="text-base font-semibold leading-7 tracking-tight text-gray-900">
                      {data.User.username}
                    </h3>
                    <p className="text-sm font-semibold leading-6 text-indigo-600">
                      {data.achievement}
                    </p>
                  </div>
                </div>
              </li>
            ))}
        </ul>
      </div>
      <div
        className="absolute left-1/2 top-0 -z-10 -translate-x-1/2 blur-3xl xl:-top-6"
        aria-hidden="true"
      >
        <div
          className="aspect-[1155/678] w-[72.1875rem] bg-gradient-to-tr from-[#ff80b5] to-[#9089fc] opacity-50"
          style={{
            clipPath:
              "polygon(74.1% 44.1%, 100% 61.6%, 97.5% 26.9%, 85.5% 0.1%, 80.7% 2%, 72.5% 32.5%, 60.2% 62.4%, 52.4% 68.1%, 47.5% 58.3%, 45.2% 34.5%, 27.5% 76.7%, 0.1% 64.9%, 17.9% 100%, 27.6% 76.8%, 76.1% 97.7%, 74.1% 44.1%)",
          }}
        />
      </div>
    </div>
  );
}
