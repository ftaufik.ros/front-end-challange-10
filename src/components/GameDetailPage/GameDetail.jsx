/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react-hooks/exhaustive-deps */
"use client";

import { fetchGamesById } from "@/redux/features/gameByIdReducer";
import { fetchLeaderboard } from "@/redux/features/leaderboardListReducer";
import { useAppDispatch, useAppSelector } from "@/redux/hook";
import Link from "next/link";
import { useEffect, useState } from "react";

import { config } from "@fortawesome/fontawesome-svg-core";
import "@fortawesome/fontawesome-svg-core/styles.css";
config.autoAddCss = false;

import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function GameDetail({ gameId }) {
  const dispatch = useAppDispatch();

  const game = useAppSelector((state) => state.gamesById.data);
  const leaderboards = useAppSelector((state) => state.leaderboard.data);

  const newLead = [...leaderboards];

  const [seeMore, setSeeMore] = useState("See More....");
  const [Spinner, setSpinner] = useState("");

  useEffect(() => {
    dispatch(fetchGamesById(gameId));
    dispatch(fetchLeaderboard());
  }, [dispatch]);

  const handleClick = () => {
    setSeeMore("");
    setSpinner(faSpinner);
  };

  function updateAchievement(score) {
    if (score < 50) {
      return "Newcomers";
    } else if (score < 100) {
      return "Bronze Rank";
    } else if (score < 200) {
      return "Silver Rank";
    } else if (score < 300) {
      return "Gold Rank";
    } else if (score < 500) {
      return "Platinum Rank";
    } else if (score < 1000) {
      return "Diamond Rank";
    } else if (score < 1500) {
      return "Ruby Rank";
    } else {
      return "Legend Rank";
    }
  }

  return (
    <div className="container px-5 lg:px-20 flex flex-col mx-auto">
      <div
        className="h-96 flex justify-center items-center text-center font-extrabold text-6xl uppercase"
        style={{
          backgroundImage: `url(${game.thumbnail})`,
          backgroundRepeat: "no-repeat",
          backgroundPosition: "center",
          backgroundSize: "cover",
          letterSpacing: "2px",
          textShadow: "3px 3px 4px black",
          color: "goldenrod",
        }}
      >
        {game.title}
      </div>

      <div className="flex flex-col lg:flex-row justify-center gap-6">
        <div className="lg:basis-1/2">
          <div className="text-yellow-400 text-3xl text-center py-2.5 bg-gray-900 h-16">
            Game Info
          </div>
          <h1 className="text-2xl text-white mt-10 mb-5">
            What is {game.title} ?
          </h1>
          <p
            className="text-xl text-white pe-5"
            style={{
              letterSpacing: "1px",
            }}
          >
            {game.description}
          </p>

          <h1 className="text-2xl text-white mt-10 mb-5">How to play ?</h1>
          <p
            className="text-xl text-white pe-5"
            style={{
              letterSpacing: "1px",
            }}
          >
            {game.how_to_play}
          </p>

          <h1 className="text-2xl text-white mt-10 mb-5">Release Date</h1>
          <p
            className="text-xl text-white"
            style={{
              letterSpacing: "1px",
            }}
          >
            {game.release_date}
          </p>

          <h1 className="text-2xl text-white mt-10 mb-5">Latest Update</h1>
          <p
            className="text-xl text-white"
            style={{
              letterSpacing: "1px",
            }}
          >
            {game.latest_update}
          </p>
        </div>

        <div className="lg:basis-1/2">
          <div className="text-yellow-400 text-3xl text-center py-2.5 bg-gray-900 h-16">
            Top 5 Leaderboard
          </div>
          {newLead
            .filter((data) => data.gameId === parseInt(gameId))
            .sort((a, b) => b.score - a.score)
            .map(
              (data, index) =>
                index < 5 && (
                  <div
                    key={index}
                    className="my-5 bg-gradient-to-r from-purple-500 to-pink-500 rounded-xl items-center flex text-yellow-500 font-bold"
                    style={{
                      textShadow: "1px 1px black",
                    }}
                  >
                    <div className="flex-auto w-24 text-center">
                      <p>{index + 1}</p>
                    </div>

                    <div
                      className="flex-auto flex-col w-52 ps-10"
                      style={{
                        letterSpacing: "1px",
                      }}
                    >
                      <Link href={`/profile/${data.userId}`}>
                        {data.User.username}
                      </Link>
                      <p className="text-white">
                        {updateAchievement(data.score)}
                      </p>
                    </div>

                    <div className="flex-auto w-24 text-center">
                      <p>{data.score}</p>
                    </div>
                  </div>
                )
            )}
          <Link
            href={"/leaderboard"}
            className="flex w-full justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-lg font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
            style={{
              letterSpacing: "1px",
            }}
            onClick={handleClick}
          >
            {seeMore} <FontAwesomeIcon icon={Spinner} spin />
          </Link>
        </div>
      </div>
    </div>
  );
}
