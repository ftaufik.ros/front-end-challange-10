"use client";

import React, { useState, useEffect } from "react";
import { useAppDispatch, useAppSelector } from "@/redux/hook";
import axios from "axios";
import RPSGame from "./RPSGame";
import Cookies from "js-cookie";
import {
  playRPSGame,
  updateScore,
  updateInfo,
  finishGame,
  updateRound,
} from "../../../redux/features/games/RPSgameReducer";

const choices = ["rock", "paper", "scissors"];

const RPSLogic = () => {
  const dispatch = useAppDispatch();
  const { currentRound, playerScore, botScore, playerwinRound, botwinRound } =
    useAppSelector((state) => state.rpsgame);

  useEffect(() => {
    if (currentRound > 0) {
      dispatch(updateInfo(`PLAYER ${playerwinRound} - COM ${botwinRound}`));
    }
  }, [dispatch, botwinRound, playerwinRound, currentRound]);

  useEffect(() => {
    if (currentRound === 3) {
      const playerWins = parseInt(playerwinRound);
      const botWins = parseInt(botwinRound);

      if (playerWins > botWins) {
        dispatch(updateScore({ playerScore: 1, botScore: 0 }));
        dispatch(finishGame("PLAYER WINNING AGAINST COM"));
      } else if (playerWins < botWins) {
        dispatch(updateScore({ playerScore: 0, botScore: 1 }));
        dispatch(finishGame("PLAYER LOST THE GAME"));
      } else {
        dispatch(updateScore({ playerScore: 0, botScore: 0 }));
        dispatch(finishGame("TIED, NEITHER PLAYER NOR COM WINS"));
      }
    }
  }, [currentRound, playerwinRound, botwinRound, dispatch]);

  const playGame = (choice) => {
    if (currentRound < 3) {
      const computerChoice = getRandomChoice();
      const result = determineResult(choice, computerChoice);
      dispatch(
        playRPSGame({
          playerChoice: choice,
          botChoice: computerChoice,
          gameInfo: result,
        })
      );
    } else if (currentRound === 3) {
      dispatch(finishGame(`${playerScore} - ${botScore}`));
    }
  };

  const getRandomChoice = () => {
    const randomIndex = Math.floor(Math.random() * choices.length);
    return choices[randomIndex];
  };

  const determineResult = (playerChoice, botChoice) => {
    if (playerChoice === botChoice) {
      dispatch(updateRound({ playerwinRound: 0, botwinRound: 0 }));
      return "It's a tie!";
    } else if (
      (playerChoice === "rock" && botChoice === "scissors") ||
      (playerChoice === "paper" && botChoice === "rock") ||
      (playerChoice === "scissors" && botChoice === "paper")
    ) {
      const result = "You win!\n+10 pts";
      if (result === "You win!\n+10 pts") {
        sendScoreToAPI();
      }
      dispatch(updateRound({ playerwinRound: 1, botwinRound: 0 }));
      return result;
    } else {
      dispatch(updateRound({ playerwinRound: 0, botwinRound: 1 }));
      return "Computer wins!";
    }
  };

  const sendScoreToAPI = async () => {
    try {
      const userId = Cookies.get("userId");
      await axios.post(`https://gatots-api.vercel.app/api/v1/score/add/${userId}/1`);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <>
      <RPSGame playGame={playGame} choices={choices} />
    </>
  );
};

export default RPSLogic;
