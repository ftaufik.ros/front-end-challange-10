'use client'

import React from "react";
import Image from "next/image";
import rock from '../../../../public/assets/batu.png';
import paper from '../../../../public/assets/kertas.png';
import scissor from '../../../../public/assets/gunting.png';
import refresh from '../../../../public/assets/refresh.png';
import './RPSGame.css';
import { useAppDispatch, useAppSelector } from "@/redux/hook";
import { resetGame } from '../../../redux/features/games/RPSgameReducer'

function RPSGame({
    playGame,
    choices,
}) {

    const dispatch = useAppDispatch()
    const {
      gameInfo,
      playerChoice,
      botChoice,
      isGameStarted,
      isGameFinished,
      currentRound,
      playerScore,
      botScore,
  } = useAppSelector((state) => state.rpsgame)
     const isDisabled = currentRound === 3

  return (
    <div className="flex flex-col items-center justify-center h-screen bg-cover bg-center" style={{ background: '#9C835F' }}>
      <div id="RPS-Game" className="container">
        <div id="game-contents" className="flex">
          <div id="game-content-left" className="w-1/4">
            <h1 id="player" className="text-2xl font-bold mb-4">PLAYER</h1>
            {playerScore > 0 || botScore > 0 ? (
            <h4 className="text-lg font-bold text-center">Score: {playerScore}</h4>
            ) : null}
            <div id="player-choice">
              {choices.map((choice) => (
                <button
                  key={choice}
                  className={`initial-state ${playerChoice === choice ? 'activeChoice' : ''} ${isDisabled ? 'disabled' : ''}`}
                  disabled={isDisabled}
                  onClick={() => playGame(choice)}
                >
                  <Image src={getImageByChoice(choice)} alt={choice} />
                </button>
              ))}
            </div>
          </div>
          <div id="game-content-center" className="w-1/4">
            <h2
            id="result"
            className={`resultC text-center ${gameInfo === 'You win!\n+10 pts' ? 'winResult' : gameInfo === 'Computer wins!' ? 'loseResult' : gameInfo === `It's a tie!` ? 'drawResult' : ''}`}
            >
            {
                !isGameStarted && !isGameFinished ? 'VS' :
                isGameStarted && !isGameFinished ? gameInfo :
                isGameStarted && isGameFinished ? `${playerScore} - ${botScore}` :
                `${playerScore} - ${botScore}`
            }
            </h2>
        </div>
          <div id="game-content-right" className="w-1/4">
            <h1 id="com" className="text-2xl font-bold mb-4">COM</h1>
            {playerScore > 0 || botScore > 0 ? (
            <h4 className="text-lg font-bold text-center">Score: {botScore}</h4>
            ) : null}
            <div id="com-choice">
              {choices.map((choice) => (
                <button
                  key={choice}
                  className={`disabled initial-state ${botChoice === choice ? 'activeChoice' : ''}`}
                >
                  <Image src={getImageByChoice(choice)} alt={choice} />
                </button>
              ))}
            </div>
          </div>
        </div>
        <div className="flex">
          <div className="w-full text-center">
            <Image
              id="retry-button"
              className={` ${currentRound === 3 ? '' : 'invisible disabled'}`}
              src={refresh}
              alt="Refresh"
              onClick={() => {
                dispatch(resetGame())
              }}
            />
          </div>
        </div>
      </div>
    </div>
  );
}

const getImageByChoice = (choice) => {
  switch (choice) {
    case 'rock':
      return rock;
    case 'paper':
      return paper;
    case 'scissors':
      return scissor;
    default:
      return '';
  }
};

export default RPSGame;