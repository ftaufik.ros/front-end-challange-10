"use client";

import React from "react";
import { useAppDispatch, useAppSelector } from "@/redux/hook";
import { resetGame } from "@/redux/features/games/GuessNumberReducer";
import GuessNumber from "./GuessNumber";

function NumberGame({ handleSubmit, handleInputChange }) {
  const dispatch = useAppDispatch();
  const {
    gameInfo,
    isGameStarted,
    isGameFinished,
    maxTries,
    currentTries,
    guess,
    randomNumber,
  } = useAppSelector((state) => state.guessnumber);

  return (
    <div className="flex flex-col items-center justify-center min-h-screen">
      <div className="relative w-full h-full">
        <div className="absolute inset-0 bg-opacity-75 bg-black" />
      </div>
      <h1 className="text-4xl font-bold mb-4">Number Guessing Game</h1>
      <p>Pick a number between 1 and 100</p>
      {isGameStarted && <p>Tries left: {maxTries - currentTries}</p>}
      <form onSubmit={handleSubmit}>
        <input
          type="number"
          value={guess}
          onChange={handleInputChange}
          min="1"
          max="100"
          required
          className="w-64 px-4 py-2 mb-4"
        />
        <button
          type="submit"
          className="px-6 py-2 bg-green-500 text-white rounded-md cursor-pointer"
          disabled={currentTries > 9 || isGameFinished}
        >
          Guess
        </button>
        {isGameFinished ? (
          <button
            className="px-6 py-2 bg-blue-500 text-white rounded-md cursor-pointer"
            onClick={() => dispatch(resetGame())}
          >
            Reset
          </button>
        ) : null}
      </form>
      {isGameStarted ? (
        <h4 className="text-lg font-bold text-center">{gameInfo}</h4>
      ) : null}
    </div>
  );
}

export default NumberGame;
