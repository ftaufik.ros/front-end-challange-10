"use client";
import { useState } from "react";
import Link from "next/link";

import { config } from "@fortawesome/fontawesome-svg-core";
import "@fortawesome/fontawesome-svg-core/styles.css";
config.autoAddCss = false;

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function ProfileEdit({
  user,
  editUsername,
  editEmail,
  editPassword,
  formData,
  handleInputChange,
  handleEditUsernameToggle,
  handleEditEmailToggle,
  handleEditPasswordToggle,
  handleUpdateUsername,
  handleUpdateEmail,
  handleUpdatePassword,
  saveButton,
  Spinner
}) {
  const handleSubmitUsername = (e) => {
    e.preventDefault();
    handleUpdateUsername(e);
  };

  const handleSubmitEmail = (e) => {
    e.preventDefault();
    handleUpdateEmail(e);
  };

  const handleSubmitPassword = (e) => {
    e.preventDefault();
    handleUpdatePassword(e);
  };

  return (
    <div
      className="flex flex-col items-center h-screen bg-cover bg-center absolute inset-0"
      style={{
        backgroundImage: `url('/assets/main-bg.jpg')`,
        zIndex: 0,
      }}
    >
      <style>
        {`
          .placeholder-black::placeholder {
            color: black;
          }
        `}
      </style>
      <div className="w-full max-w-md p-6 bg-yellow-700 bg-opacity-80 mt-20 sm:rounded-3xl relative">
        <div
          className="absolute inset-0 bg-gradient-to-r from-yellow-300 to-yellow-600 shadow-lg transform -skew-y-6 sm:skew-y-0 sm:-rotate-6 sm:rounded-3xl"
          style={{ zIndex: -1 }}
        ></div>

        <div className="flex justify-between items-center mb-4">
          <h2 className="text-2xl text-white font-bold">EDIT USER</h2>
          <Link href="/profile">
            <span className="text-white hover:text-yellow-400 transition duration-500 cursor-pointer">
              Back to Profile
              <i className="fas fa-chevron-circle-left fa-fw"></i>
            </span>
          </Link>
        </div>
        <form onSubmit={handleSubmitUsername}>
          <div className="form-group pb-4">
            <div className="label-wrapper" style={{ paddingBottom: "0.3rem" }}>
              <label
                htmlFor="username"
                className="form-label font-bold text-white"
              >
                Username
              </label>
            </div>
            <div
              className="input-wrapper "
              style={{ paddingBottom: "0.75rem" }}
            >
              <input
                type="text"
                className={`form-control rounded-md w-full ${
                  editUsername ? "bg-white" : "bg-gray-300"
                }`}
                id="username"
                placeholder="Enter username"
                name="username"
                value={formData.username || (user && user.username) || ""}
                onChange={handleInputChange}
                disabled={!editUsername}
                style={{ padding: "0.3rem" }}
              />
            </div>
            {editUsername ? (
              <div className="btn-group">
                <button
                  type="submit"
                  className="inline-flex items-center px-4 py-2 border border-transparent text-base leading-6 font-medium rounded-full text-white bg-yellow-500 hover:bg-yellow-600 focus:outline-none focus:border-yellow-700 focus:shadow-outline-yellow active:bg-yellow-700 transition ease-in-out duration-150 cursor-pointer"
                  style={{ marginRight: "0.5rem" }}
                >
                  Save
                </button>
                <button
                  type="button"
                  className="inline-flex items-center px-4 py-2 border border-transparent text-base leading-6 font-medium rounded-full text-white bg-yellow-500 hover:bg-yellow-600 focus:outline-none focus:border-yellow-700 focus:shadow-outline-yellow active:bg-yellow-700 transition ease-in-out duration-150 cursor-pointer"
                  onClick={handleEditUsernameToggle}
                >
                  Cancel
                </button>
              </div>
            ) : (
              <button
                type="button"
                className="inline-flex items-center justify-center px-4 py-2 border border-transparent text-base leading-6 font-medium rounded-full text-white bg-yellow-500 hover:bg-yellow-600 focus:outline-none focus:border-yellow-700 focus:shadow-outline-yellow active:bg-yellow-700 transition ease-in-out duration-150 cursor-pointer"
                onClick={handleEditUsernameToggle}
                style={{
                  width: "100%",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                Edit Username
              </button>
            )}
          </div>
        </form>
        <form onSubmit={handleSubmitEmail}>
          <div className="form-group pb-4">
            <div className="label-wrapper" style={{ paddingBottom: "0.3rem" }}>
              <label
                htmlFor="email"
                className="form-label font-bold text-white"
              >
                Email Address
              </label>
            </div>
            <div className="input-wrapper" style={{ paddingBottom: "0.75rem" }}>
              <input
                type="email"
                className={`form-control rounded-md w-full ${
                  editEmail ? "bg-white" : "bg-gray-300"
                }`}
                id="email"
                placeholder="Enter email"
                name="email"
                value={formData.email || (user && user.email) || ""}
                onChange={handleInputChange}
                disabled={!editEmail}
                style={{ padding: "0.3rem" }}
              />
            </div>
            {editEmail ? (
              <div className="btn-group">
                <button
                  type="submit"
                  className="inline-flex items-center px-4 py-2 border border-transparent text-base leading-6 font-medium rounded-full text-white bg-yellow-500 hover:bg-yellow-600 focus:outline-none focus:border-yellow-700 focus:shadow-outline-yellow active:bg-yellow-700 transition ease-in-out duration-150 cursor-pointer"
                  style={{ marginRight: "0.5rem" }}
                >
                  Save
                </button>
                <button
                  type="button"
                  className="inline-flex items-center px-4 py-2 border border-transparent text-base leading-6 font-medium rounded-full text-white bg-yellow-500 hover:bg-yellow-600 focus:outline-none focus:border-yellow-700 focus:shadow-outline-yellow active:bg-yellow-700 transition ease-in-out duration-150 cursor-pointer"
                  onClick={handleEditEmailToggle}
                >
                  Cancel
                </button>
              </div>
            ) : (
              <button
                type="button"
                className="inline-flex items-center justify-center px-4 py-2 border border-transparent text-base leading-6 font-medium rounded-full text-white bg-yellow-500 hover:bg-yellow-600 focus:outline-none focus:border-yellow-700 focus:shadow-outline-yellow active:bg-yellow-700 transition ease-in-out duration-150 cursor-pointer"
                onClick={handleEditEmailToggle}
                style={{
                  width: "100%",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                Edit Email
              </button>
            )}
          </div>
        </form>
        <form onSubmit={handleSubmitPassword}>
          <div className="form-group">
            <div className="label-wrapper" style={{ paddingBottom: "0.3rem" }}>
              <label
                htmlFor="oldPassword"
                className="form-label font-bold text-white"
              >
                Password
              </label>
            </div>
            <div className="input-wrapper">
              <input
                type="password"
                className={`form-control rounded-md w-full placeholder-black ${
                  editPassword ? "bg-white" : "bg-gray-300"
                }`}
                id="oldPassword"
                placeholder=""
                name="oldPassword"
                value={formData.password.oldPassword}
                onChange={handleInputChange}
                disabled={!editPassword}
                style={{ padding: "0.3rem" }}
                required
              />
            </div>
            <div className="label-wrapper" style={{ paddingBottom: "0.3rem" }}>
              <label
                htmlFor="newPassword"
                className="form-label font-bold text-white"
              >
                New Password
              </label>
            </div>
            <div className="input-wrapper" style={{ paddingBottom: "0.75rem" }}>
              <input
                type="password"
                className={`form-control rounded-md w-full placeholder-black ${
                  editPassword ? "bg-white" : "bg-gray-300"
                }`}
                id="newPassword"
                placeholder=""
                name="newPassword"
                value={formData.password.newPassword}
                onChange={handleInputChange}
                disabled={!editPassword}
                style={{ padding: "0.3rem" }}
                required
              />
            </div>

            {editPassword ? (
              <div className="btn-group">
                <button
                  type="submit"
                  className="inline-flex items-center px-4 py-2 border border-transparent text-base leading-6 font-medium rounded-full text-white bg-yellow-500 hover:bg-yellow-600 focus:outline-none focus:border-yellow-700 focus:shadow-outline-yellow active:bg-yellow-700 transition ease-in-out duration-150 cursor-pointer"
                  style={{ marginRight: "0.5rem" }}
                >
                  {saveButton}  <FontAwesomeIcon icon={Spinner} spin />
                </button>
                <button
                  type="button"
                  className="inline-flex items-center px-4 py-2 border border-transparent text-base leading-6 font-medium rounded-full text-white bg-yellow-500 hover:bg-yellow-600 focus:outline-none focus:border-yellow-700 focus:shadow-outline-yellow active:bg-yellow-700 transition ease-in-out duration-150 cursor-pointer"
                  onClick={handleEditPasswordToggle}
                >
                  Cancel
                </button>
              </div>
            ) : (
              <button
                type="button"
                className="inline-flex items-center justify-center px-4 py-2 border border-transparent text-base leading-6 font-medium rounded-full text-white bg-yellow-500 hover:bg-yellow-600 focus:outline-none focus:border-yellow-700 focus:shadow-outline-yellow active:bg-yellow-700 transition ease-in-out duration-150 cursor-pointer"
                onClick={handleEditPasswordToggle}
                style={{
                  width: "100%",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                Edit Password
              </button>
            )}
          </div>
        </form>
      </div>
    </div>
  );
}

export default ProfileEdit;
