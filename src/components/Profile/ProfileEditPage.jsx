"use client";
import { useState, useEffect } from "react";
import axios from "axios";
import Cookie from "js-cookie";
import ProfileEdit from "../Profile/ProfileEdit";
import Navbar from "../Navbar/Navbar"

import { config } from "@fortawesome/fontawesome-svg-core";
import "@fortawesome/fontawesome-svg-core/styles.css";
config.autoAddCss = false;

import { faSpinner } from "@fortawesome/free-solid-svg-icons";

function ProfileEditPage() {
  const [user, setUser] = useState({});
  const [load, setLoad] = useState(true);
  const [editUsername, setEditUsername] = useState(false);
  const [editEmail, setEditEmail] = useState(false);
  const [editPassword, setEditPassword] = useState(false);
  const [formData, setFormData] = useState({
    username: "",
    email: "",
    password: "",
  });

  const [saveButton, setSaveButton] = useState("Save")
  const [spinner, setSpinner] = useState('')

  const loadUser = async () => {
    try {
      let userId = Cookie.get("userId");
      const response = await axios.get(
        `https://gatots-api.vercel.app/api/v1/user/${userId}`
      );
      setUser(response.data.data);
      setLoad(false);
    } catch (err) {
      console.log(err);
    }
  };

  const handleInputChange = (event) => {
    setFormData((prevFormData) => ({
      ...prevFormData,
      [event.target.name]: event.target.value,
    }));
    console.log(formData);
  };

  const handleEditUsernameToggle = () => {
    setEditUsername(!editUsername);
    if (user) {
      setFormData((prevFormData) => ({
        ...prevFormData,
        username: user.username,
        email: user.email,
      }));
    }
  };

  const handleEditEmailToggle = () => {
    setEditEmail(!editEmail);
    if (user) {
      setFormData((prevFormData) => ({
        ...prevFormData,
        username: user.username,
        email: user.email,
      }));
    }
  };

  const handleEditPasswordToggle = () => {
    setEditPassword(!editPassword);
    if (user) {
      setFormData((prevFormData) => ({
        ...prevFormData,
        username: user.username,
        email: user.email,
      }));
    }
  };

  const handleUpdateUsername = async (event) => {
    event.preventDefault();
    try {
      setSaveButton("")
      setSpinner(faSpinner)

      let userId = Cookie.get("userId");
      const updatedUser = {
        username: formData.username || user.username,
      };

      const response = await axios.put(
        `https://gatots-api.vercel.app/api/v1/user/edit/${userId}`,
        updatedUser
      );
      console.log(response.data);
      setUser(response.data.data);
      setEditUsername(false);
    } catch (err) {
      console.log(err);
    }
  };

  const handleUpdateEmail = async (event) => {
    event.preventDefault();
    try {
      let userId = Cookie.get("userId");
      const updatedUser = {
        email: formData.email || user.email,
      };

      const response = await axios.put(
        `https://gatots-api.vercel.app/api/v1/user/edit/${userId}`,
        updatedUser
      );
      console.log(response.data);
      setUser(response.data.data);
      setEditEmail(false);
    } catch (err) {
      console.log(err);
    }
  };

  const handleUpdatePassword = async (event) => {
    event.preventDefault();
    try {
      let userId = Cookie.get("userId");
      const updatedUser = {
        oldPassword: formData.oldPassword,
        newPassword: formData.newPassword,
      };

      const response = await axios.put(
        `https://gatots-api.vercel.app/api/v1/user/edit/${userId}`,
        updatedUser
      );
      console.log(response.data);
      setUser(response.data.data);

      setSpinner('')
      setSaveButton("Saved")

      setTimeout(() => {
        setSaveButton("Save")
      }, 1000);
      
      setEditPassword(false);
    } catch (err) {
      console.log(err);

      setSpinner('')
      setSaveButton("Save")

      setTimeout(() => {
        setSaveButton("Save")
      }, 1000);
    }
  };

  useEffect(() => {
    loadUser();
  }, []);

  return (
    <div className="main">
      <div className="relative" style={{ zIndex: 999 }}>
        <Navbar />
      </div>
      {!load && (
        <ProfileEdit
          user={user}
          editUsername={editUsername}
          editEmail={editEmail}
          editPassword={editPassword}
          formData={formData}
          handleInputChange={handleInputChange}
          handleEditUsernameToggle={handleEditUsernameToggle}
          handleEditEmailToggle={handleEditEmailToggle}
          handleEditPasswordToggle={handleEditPasswordToggle}
          handleUpdateUsername={handleUpdateUsername}
          handleUpdateEmail={handleUpdateEmail}
          handleUpdatePassword={handleUpdatePassword}
          saveButton={saveButton}
          Spinner={spinner}
        />
      )}
    </div>
  );
}

export default ProfileEditPage;
