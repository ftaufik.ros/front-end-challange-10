"use client";

import Image from "next/image";
import Link from "next/link";

function Home({ user }) {
  return (
    <div
    className="flex flex-col items-center h-screen bg-cover bg-center absolute inset-0"
      style={{
        backgroundImage: `url('/assets/main-bg.jpg')`,
        position: "relative",
        zIndex: 0,
      }}>
      <h1 className="text-white font-bold text-xl mt-2 text-center">
        Welcome, {user.username}{" "}
      </h1>
      <div
        style={styles.container}
        className="max-w-sm w-full lg:max-w-full lg:flex mt-8"
      >
        <div
          style={styles.border}
          className="w-1/2 bg-white p-9 rounded-xl bg-opacity-60 backdrop-filter backdrop-blur-lg flex flex-col justify-between leading-normal"
        >
          <Image
            className="mb-6"
            src="/assets/profile-dummy.png"
            alt="Profile"
            width={48}
            height={48}
          ></Image>
          <div className="mb-8">
            <p className="text-black text-base font-bold">User Info</p>
            <p className="text-black text-base">
              Username : {user.username}
            </p>
            <p className="text-black text-base">Email : {user.email}</p>
          </div>
          <div className="flex items-center">
            <div className="bg-transparent hover:bg-gray-500 text-gray-700 font-semibold hover:text-white py-2 px-4 border border-gray-500 hover:border-transparent rounded">
              <Link href="./profile" className="leading-none">
                View My Profile
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

const styles = {
  container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  border: {
    width: "350px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
};

export default Home;
