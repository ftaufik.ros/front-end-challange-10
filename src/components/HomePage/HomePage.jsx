"use client";

import Home from "./Home";
import Cookie from "js-cookie";
import { useState, useEffect } from "react";
import axios from "axios";
import { useRouter, useSearchParams } from "next/navigation";
import { useAppDispatch } from "@/redux/hook";
import { LoginAction } from "@/redux/features/AuthReducer";

import { config } from "@fortawesome/fontawesome-svg-core";
import "@fortawesome/fontawesome-svg-core/styles.css";
config.autoAddCss = false;

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";

const HomePage = () => {
  const [user, setUser] = useState({});
  const [loading, setLoading] = useState(false);
  const dispatch = useAppDispatch();

  const getUser = async () => {
    try {
      setLoading(true);
      const router = useRouter();
      const searchParams = useSearchParams();
      let idFromUrl = searchParams.get("userId");
      let userId = Cookie.get("userId");
      if (idFromUrl !== undefined || userId !== undefined) {
        const res = await axios.get(
          `https://gatots-api.vercel.app/api/v1/user/${userId}`
        );
        const data = res.data.data;
        dispatch(LoginAction());
        setUser(data);
        setLoading(false);
      } else {
        router.push("/login");
      }
    } catch (error) {
      setLoading(false);
      console.log(error);
    }
  };

  useEffect(() => {
    getUser();
  }, []);

  if (loading) {
    return (
      <div className="flex justify-center items-center mt-48">
        <FontAwesomeIcon icon={faSpinner} spin size="2xl" />
      </div>
    );
  }

  return <Home user={user}></Home>;
};

export default HomePage;
