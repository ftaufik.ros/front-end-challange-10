"use client";

import React, { useState } from "react";
import { useRouter } from "next/navigation";
import RegisterForm from "../Register/RegisterForm";
import axios from "axios";
import Cookies from "js-cookie";
import { useAppDispatch, useAppSelector } from "@/redux/hooks";

//Font Awesome
import { config } from "@fortawesome/fontawesome-svg-core";
import "@fortawesome/fontawesome-svg-core/styles.css";
config.autoAddCss = false;

import { faSpinner } from "@fortawesome/free-solid-svg-icons";

function Register() {
  const router = useRouter();
  const loginChecker = useAppSelector((state) => state.authreducer.isLoggedIn);
  if (loginChecker) {
    router.push("/home");
  }
  const [user, setUser] = useState({
    username: "",
    email: "",
    password: "",
  });

  const [signUpButton, setSignUpButton] = useState("Sign up")
  const [spinner, setSpinner] = useState('')

  const onInputChange = (e) => {
    const { name, value } = e.target;
    setUser((prevState) => ({ ...prevState, [name]: value }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      setSignUpButton("")
      setSpinner(faSpinner)

      const res = await axios.post(
        "https://gatots-api.vercel.app/api/v1/user/register",
        user
      );
      console.log("Register success", res.data);

      setSpinner('')
      setSignUpButton("Done")
      
      router.push("/login");
    } catch (error) {

      console.log(error);

      setSpinner('')
      setSignUpButton("Sign up Failed")

      setTimeout(() => {
        setSignUpButton("Sign up")
      }, 1000);
    }
  };

  return (
    <div className="main">
      <RegisterForm
        user={user}
        onInputChange={onInputChange}
        onSubmit={handleSubmit}
        signUpButton={signUpButton}
        Spinner={spinner}
      />
    </div>
  );
}

export default Register;
