import Image from "next/image";
import googleico from "../../../public/assets/google-d.png";
import facebookico from "../../../public/assets/facebook.svg";
import twitterico from "../../../public/assets/twitter-d.png";
import githubico from "../../../public/assets/github_icon.png";
import Link from "next/link";

import { config } from "@fortawesome/fontawesome-svg-core";
import "@fortawesome/fontawesome-svg-core/styles.css";
config.autoAddCss = false;

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function LoginForm({ user, onInputChange, onSubmit, signInButton, Spinner }) {
  return (
    <>
      <div className="flex flex-1 flex-col justify-center px-6 pt-20 lg:px-8">
        <div className="sm:mx-auto sm:w-full sm:max-w-sm">
          <h2 className="text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">
            Sign in to your account
          </h2>
        </div>

        <div className="flex flex-row justify-center items-center space-x-3 mt-5">
          <Link
            href="/auth/google/home"
            className="w-12 h-12 items-center justify-center inline-flex rounded-full font-bold text-lg  text-white bg-white hover:shadow-lg cursor-pointer transition ease-in duration-300"
          >
            <Image
              className="w-8 h-8"
              src={googleico}
              width={32}
              height={32}
              alt=""
            />
          </Link>

          <Link
            href="/auth/facebook/home"
            className="w-10 h-10 items-center justify-center inline-flex rounded-full font-bold text-lg  text-white  bg-[#4267B2] hover:shadow-lg cursor-pointer transition ease-in duration-300"
          >
            <Image
              className="w-6 h-6"
              src={facebookico}
              width={24}
              height={24}
              alt=""
            />
          </Link>

          <Link
            href="/auth/twitter/home"
            className="w-10 h-10 items-center justify-center inline-flex rounded-full font-bold text-lg  text-white bg-[#1DA1F2] hover:shadow-lg cursor-pointer transition ease-in duration-300"
          >
            <Image
              className="w-6 h-6"
              src={twitterico}
              width={24}
              height={24}
              alt=""
            />
          </Link>

          <Link
            href="/auth/github/home"
            className="w-12 h-12 items-center justify-center inline-flex rounded-full font-bold text-lg  text-black bg-black hover:shadow-lg cursor-pointer transition ease-in duration-300"
          >
            <Image
              className="w-12 h-12"
              src={githubico}
              width={48}
              height={48}
              alt=""
            />
          </Link>
        </div>
        <div className="flex items-center justify-center space-x-2 mt-6">
          <span className="h-px w-16 bg-gray-300"></span>
          <span className="text-gray-500 font-normal">OR</span>
          <span className="h-px w-16 bg-gray-300"></span>
        </div>

        <div className="mt-5 sm:mx-auto sm:w-full sm:max-w-sm">
          <form className="space-y-6" onSubmit={onSubmit}>
            <div>
              <label
                htmlFor="email"
                className="block text-sm font-medium leading-6 text-gray-900"
              >
                Email address
              </label>
              <div className="mt-2">
                <input
                  id="email"
                  name="email"
                  type="email"
                  autoComplete="email"
                  value={user.email}
                  onChange={onInputChange}
                  required
                  className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                />
              </div>
            </div>

            <div>
              <div className="flex items-center justify-between">
                <label
                  htmlFor="password"
                  className="block text-sm font-medium leading-6 text-gray-900"
                >
                  Password
                </label>
                <div className="text-sm">
                  <Link
                    href="/forgotpassword"
                    className="font-semibold text-indigo-600 hover:text-indigo-500"
                  >
                    Forgot password?
                  </Link>
                </div>
              </div>
              <div className="mt-2">
                <input
                  id="password"
                  name="password"
                  type="password"
                  autoComplete="current-password"
                  value={user.password}
                  onChange={onInputChange}
                  required
                  className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                />
              </div>
            </div>

            <div>
              <button
                type="submit"
                className="flex w-full justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
              >
                {signInButton} <FontAwesomeIcon icon={Spinner} spin />
              </button>
            </div>
          </form>

          <p className="mt-10 text-center text-sm text-gray-500">
            Not a member?{" "}
            <Link
              href="/register"
              className="font-semibold leading-6 text-indigo-600 hover:text-indigo-500"
            >
              Sign Up Now
            </Link>
          </p>
        </div>
      </div>
    </>
  );
}
