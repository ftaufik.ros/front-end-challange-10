"use client";

import { Fragment, useEffect, useState } from "react";
import { Listbox, Transition } from "@headlessui/react";
import { CheckIcon, ChevronUpDownIcon } from "@heroicons/react/20/solid";
import { useAppDispatch, useAppSelector } from "@/redux/hook";
import { fetchLeaderboard } from "@/redux/features/leaderboardListReducer";
import Link from "next/link";

const games = [
  {
    id: 1,
    title: "Rock Paper Scissor",
  },
  {
    id: 2,
    title: "Guess The Number",
  },
  {
    id: 3,
    title: "Backpack Hero",
  },
  {
    id: 4,
    title: "Tetris",
  },
];

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function LeaderboardList() {
  const dispatch = useAppDispatch();

  const [selected, setSelected] = useState(games[0]);

  const leaderboards = useAppSelector((state) => state.leaderboard.data);
  const newLead = [...leaderboards];
  function updateAchievement(score) {
    if (score < 50) {
      return "Newcomers";
    } else if (score < 100) {
      return "Bronze Rank";
    } else if (score < 200) {
      return "Silver Rank";
    } else if (score < 300) {
      return "Gold Rank";
    } else if (score < 500) {
      return "Platinum Rank";
    } else if (score < 1000) {
      return "Diamond Rank";
    } else if (score < 1500) {
      return "Ruby Rank";
    } else {
      return "Legend Rank";
    }
  }

  useEffect(() => {
    dispatch(fetchLeaderboard());
  }, [dispatch]);

  return (
    <div className="w-100 mb-20">
      <div className="sm:col-span-3 ms-1 sm:ms-8 md:ms-20 w-64 mt-5">
        <Listbox value={selected} onChange={setSelected}>
          {({ open }) => (
            <>
              <Listbox.Label className="block text-sm md:text-lg font-medium leading-6 text-white">
                Games :
              </Listbox.Label>
              <div className="relative mt-2">
                <Listbox.Button className="relative w-full cursor-default rounded-md bg-white py-1.5 pl-3 pr-10 text-left text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 focus:outline-none focus:ring-2 focus:ring-indigo-500 text-sm md:text-base sm:leading-6">
                  <span className="flex items-center">
                    <span className="ml-3 block truncate font-bold ">
                      {selected.title}
                    </span>
                  </span>
                  <span className="pointer-events-none absolute inset-y-0 right-0 ml-3 flex items-center pr-2">
                    <ChevronUpDownIcon
                      className="h-5 w-5 text-gray-400"
                      aria-hidden="true"
                    />
                  </span>
                </Listbox.Button>

                <Transition
                  show={open}
                  as={Fragment}
                  leave="transition ease-in duration-100"
                  leaveFrom="opacity-100"
                  leaveTo="opacity-0"
                >
                  <Listbox.Options className="absolute z-10 mt-1 max-h-56 w-full overflow-auto rounded-md bg-white py-1 text-sm shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none md:text-base">
                    {games.map((game) => (
                      <Listbox.Option
                        key={game.id}
                        className={({ active }) =>
                          classNames(
                            active
                              ? "bg-indigo-600 text-white"
                              : "text-gray-900",
                            "relative cursor-default select-none py-2 pl-3 pr-9 font-bold"
                          )
                        }
                        value={game}
                      >
                        {({ selected, active }) => (
                          <>
                            <div className="flex items-center">
                              <span
                                className={classNames(
                                  selected ? "font-bold" : "font-normal",
                                  "ml-3 block truncate"
                                )}
                              >
                                {game.title}
                              </span>
                            </div>

                            {selected ? (
                              <span
                                className={classNames(
                                  active ? "text-white" : "text-indigo-600",
                                  "absolute inset-y-0 right-0 flex items-center pr-4"
                                )}
                              >
                                <CheckIcon
                                  className="h-5 w-5"
                                  aria-hidden="true"
                                />
                              </span>
                            ) : null}
                          </>
                        )}
                      </Listbox.Option>
                    ))}
                  </Listbox.Options>
                </Transition>
              </div>
            </>
          )}
        </Listbox>
      </div>
      <div className="flex w-100 h-12 items-center px-3 mx-1 sm:mx-8 mt-8 md:mx-20 md:mt-12 rounded-xl bg-white text-center text-sm md:text-xl text-black font-bold">
        <div className="flex-none w-10 md:w-20">No</div>
        <div className="flex-1 w-30 md:w-35">Username</div>
        <div className="flex-1 w-25 md:w-30">Rank</div>
        <div className="flex-1 w-25">Score</div>
      </div>

      {newLead
        .filter((data) => data.gameId === selected.id)
        .sort((a, b) => b.score - a.score)
        .map((data, index) => (
          <div
            key={index}
            className="flex w-100 h-12 items-center px-3 mx-1 sm:mx-8 md:mx-20 mt-4 rounded-xl bg-gradient-to-r from-purple-500 to-fuchsia-500 text-sm text-center md:text-lg font-semibold text-white"
          >
            <div className="flex-none w-10 md:w-20">{index + 1}</div>
            <div className="flex-1 w-30 md:w-35">
              <Link href={`/profile/${data.userId}`}>{data.User.username}</Link>
            </div>
            <div className="flex-1 w-25 md:w-30">
              {updateAchievement(data.score)}
            </div>
            <div className="flex-1 w-25">{data.score}</div>
          </div>
        ))}
    </div>
  );
}
