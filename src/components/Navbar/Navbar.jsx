/* eslint-disable react-hooks/exhaustive-deps */
"use client";

import Link from "next/link";
import Image from "next/image";
import { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "@/redux/hook";
import { LoginAction } from "@/redux/features/AuthReducer";
import Cookies from "js-cookie";

const Navbar = () => {
  const dispatch = useAppDispatch();
  const isLoggedIn = useAppSelector((state) => state.authreducer.isLoggedIn);
  let userId = Cookies.get("userId");

  const AuthCheck = () => {
    if (userId !== undefined) {
      dispatch(LoginAction());
    }
  };

  useEffect(() => {
    AuthCheck();
  }, [userId]);

  return (
    <div className="bg-black text-white">
      <nav className="container mx-auto px-4 py-2 flex justify-between items-center">
        <div>
          <Link href="/" legacyBehavior>
            <Image
              src="/assets/icon-web.webp"
              alt="Logo"
              width={48}
              height={48}
            />
          </Link>
        </div>
        <div className="flex items-center justify-center flex-grow">
          <ul className="flex space-x-4">
            <li>
              <Link
                href="/home"
                className="text-yellow-500 hover:text-gray-300"
              >
                Home
              </Link>
            </li>
            <li>
              <Link
                href="/games"
                className="text-yellow-500 hover:text-gray-300"
              >
                Games
              </Link>
            </li>
            <li>
              <Link
                href="/leaderboard"
                className="text-yellow-500 hover:text-gray-300"
              >
                Leaderboard
              </Link>
            </li>
          </ul>
        </div>
        <div>
          <ul className="flex space-x-4">
            {isLoggedIn ? (
              <>
                <li>
                  <Link
                    href="/profile"
                    className="text-yellow-500 hover:text-gray-300"
                  >
                    Profile
                  </Link>
                </li>
                <li>
                  <Link
                    href="/logout"
                    className="text-yellow-500 hover:text-gray-300"
                  >
                    Logout
                  </Link>
                </li>
              </>
            ) : (
              <>
                <li>
                  <Link
                    href="/register"
                    className="text-yellow-500 hover:text-gray-300"
                  >
                    Sign Up
                  </Link>
                </li>
                <li>
                  <Link
                    href="/login"
                    className="text-yellow-500 hover:text-gray-300"
                  >
                    Sign In
                  </Link>
                </li>
              </>
            )}
          </ul>
        </div>
      </nav>
    </div>
  );
};

export default Navbar;
