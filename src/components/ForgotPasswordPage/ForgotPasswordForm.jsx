"use client";
import React from "react";

import { config } from "@fortawesome/fontawesome-svg-core";
import "@fortawesome/fontawesome-svg-core/styles.css";
config.autoAddCss = false;

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function ForgotPasswordForm({
  email,
  onSubmit,
  onInputChange,
  ShowResults,
  submit,
  resetButton,
  Spinner
}) {
  return (
    <div className="flex flex-1 flex-col justify-center px-6 pt-20 lg:px-8">
      <div className="mt-5 sm:mx-auto sm:w-full sm:max-w-sm">
        <form className="space-y-6" onSubmit={onSubmit}>
          <h2 className="text-sm text-center mb-4 text-black">
            Enter your registered email
          </h2>
          <div>
            <label
              htmlFor="email"
              className="block text-sm font-medium leading-6 text-gray-900"
            >
              Email address
            </label>
            <div className="mt-2">
              <input
                id="email"
                name="email"
                type="email"
                autoComplete="email"
                value={email}
                onChange={onInputChange}
                required
                className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
              />
            </div>
          </div>
          <button
            type="submit"
            className="reset-button w-full mt-3 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
          >
            {resetButton}  <FontAwesomeIcon icon={Spinner} spin />
          </button>
        </form>
        {submit && <ShowResults />}
      </div>
    </div>
  );
}

export default ForgotPasswordForm;
