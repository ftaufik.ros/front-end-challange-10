"use client";

import React, { useState } from "react";
import ResetPasswordForm from "./ResetPasswordForm";
import axios from "axios";

//Font Awesome
import { config } from "@fortawesome/fontawesome-svg-core";
import "@fortawesome/fontawesome-svg-core/styles.css";
config.autoAddCss = false;

import { faSpinner } from "@fortawesome/free-solid-svg-icons";

function ResetPasswordPage() {
  const [data, setData] = useState({
    resetToken: "",
    password: "",
  });

  const [submit, setSubmit] = useState(false);

  const [resetButton, setResetButton] = useState("Reset")
  const [spinner, setSpinner] = useState('')

  const onInputChange = (e) => {
    const { name, value } = e.target;
    setData((prevState) => ({ ...prevState, [name]: value }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      setResetButton("")
      setSpinner(faSpinner)

      const res = await axios.post(
        "https://gatots-api.vercel.app/api/v1/user/reset-password",
        data
      );

      setSpinner('')
      setResetButton("Done")
      
      console.log(res.data.msg);
      setSubmit(true);
    } catch (err) {
      console.log(err);

      setSpinner('')
      setResetButton("Reset Password Failed")

      setTimeout(() => {
        setResetButton("Reset")
      }, 1000);
    }
  };

  function ShowResults() {
    return (
      <div style={styles.resultContainer}>
        <h4>
          Reset Password Success, you can go try login again with your new
          password.
        </h4>
        <a href="/login">LOGIN HERE</a>
      </div>
    );
  }
  return (
    <div className="main">
      <ResetPasswordForm
        data={data}
        onInputChange={onInputChange}
        onSubmit={handleSubmit}
        ShowResults={ShowResults}
        submit={submit}
        resetButton={resetButton}
        Spinner={spinner}
      />
    </div>
  );
}

const styles = {
  resultContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
};

export default ResetPasswordPage;
