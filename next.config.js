/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    domains: [
      "images.unsplash.com",
      "i.ibb.co",
      "easyshiksha.com",
      "cdn.akamai.steamstatic.com",
      "play-lh.googleusercontent.com",
    ],
  },
};

module.exports = nextConfig;
